package jinter_a1;

public class Alien {
	protected int numeyes;
	protected int nummouth;
	private int numears;
	
	public Alien(int numeyes, int nummouth, int numears)
	{
		this.numeyes = numeyes;
		this.nummouth = nummouth;
		this.numears = numears;
	}
	
	public String toString()
	{
		return "number of eyes: " + new Integer(numeyes).toString() + ", number of mouths: " + new Integer(nummouth).toString() + ", number of ears:  " + new Integer(numears).toString();
	}
}
