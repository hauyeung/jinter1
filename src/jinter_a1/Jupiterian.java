package jinter_a1;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Jupiterian extends Alien {
	
	public Jupiterian()
	{
		super(2,2,2);
	}
	
	public void draw(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		Font font = new Font("Serif", Font.PLAIN, 15);
		g2.setFont(font);
		g2.drawOval(180,10, 90,90);
		for (int i=1; i <= super.numeyes; i++)
		{
			g2.drawOval(205 +30*(i-1), 30,10,10);
		}
		
		for (int i=1; i <= super.nummouth; i++)
		{
			g2.drawOval(202+30*(i-1), 50, 20, 10);
		}
		
		g2.drawString("Jupiterian", 195,130);
		
	}

}
